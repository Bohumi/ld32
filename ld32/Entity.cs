﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ld32
{
	abstract class Entity : IMoveable, IDrawable
	{
		public abstract void Draw();
		public abstract void Move(int dx, int dy);
		public abstract void MoveTo(int x, int y);
	}
}
