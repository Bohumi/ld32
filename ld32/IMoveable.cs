﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ld32
{
	interface IMoveable
	{
		void Move(int dx, int dy);
		void MoveTo(int x, int y);
	}
}
