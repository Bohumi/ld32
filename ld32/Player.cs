﻿namespace ld32
{
	class Player : Entity
	{
		private int x, y;

		public int X
		{
			get { return x; }
			private set { x = value; }
		}

		public int Y
		{
			get { return x; }
			private set { x = value; }
		}

		public override void Move(int dx, int dy)
		{
			X += dx;
			Y += dy;
		}

		public override void MoveTo(int x, int y)
		{
			X = x;
			Y = y;
		}

		public override void Draw()
		{

		}
	}
}